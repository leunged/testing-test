package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmailValidatorTest {

	@Test
	public void testIsValidFormatRegular() {
		boolean result = EmailValidator.isValidFormat("test@example.com");
		assertTrue("Invalid format", result);
	}
	
	@Test
	public void testIsValidFormatException() {
		boolean result = EmailValidator.isValidFormat("@testexample.com");
		assertFalse("Invalid format", result);
	}
	
	@Test
	public void testIsValidAtSymbolRegular() {
		boolean result = EmailValidator.isValidAtSymbol("test@example.com");
		assertTrue("Invalid format", result);
	}
	
	@Test
	public void testIsValidAtSymbolException() {
		boolean result = EmailValidator.isValidAtSymbol("test@@example.com");
		assertFalse("Invalid format", result);
	}
	
	@Test
	public void testIsValidAccountNameRegular() {
		boolean result = EmailValidator.isValidAccountName("test@example.com");
		assertTrue("Invalid account name", result);
	}
	
	@Test
	public void testIsValidAccountNameException() {
		boolean result = EmailValidator.isValidAccountName("@example.com");
		assertFalse("Invalid account name", result);
	}
	
	@Test
	public void testIsValidAccountNameBoundaryIn() {
		boolean result = EmailValidator.isValidAccountName("tes@example.com");
		assertTrue("Invalid account name", result);
	}
	
	@Test
	public void testIsValidAccountNameBoundaryOut() {
		boolean result = EmailValidator.isValidAccountName("te@example.com");
		assertFalse("Invalid account name", result);
	}
	
	@Test
	public void testIsValidDomainNameRegular() {
		boolean result = EmailValidator.isValidDomainName("test@example.com");
		assertTrue("Invalid domain name", result);
	}
	
	@Test
	public void testIsValidDomainNameException() {
		boolean result = EmailValidator.isValidDomainName("test@.com");
		assertFalse("Invalid domain name", result);
	}
	
	@Test
	public void testIsValidDomainNameBoundaryIn() {
		boolean result = EmailValidator.isValidDomainName("test@exa.com");
		assertTrue("Invalid domain name", result);
	}
	
	@Test
	public void testIsValidDomainNameBoundaryOut() {
		boolean result = EmailValidator.isValidDomainName("test@12.com");
		assertFalse("Invalid domain name", result);
	}

	@Test
	public void testIsExtensionNameRegular() {
		boolean result = EmailValidator.isValidExtensionName("test@example.com.corp");
		assertTrue("Invalid extension name", result);
	}
	
	@Test
	public void testIsExtensionNameRBoundary() {
		boolean result = EmailValidator.isValidExtensionName("test@example.com.12");
		assertFalse("Invalid extension name", result);
	}
	
	@Test
	public void testIsExtensionNameBoundaryIn() {
		boolean result = EmailValidator.isValidExtensionName("test@example.com.in");
		assertTrue("Invalid extension name", result);
	}

}